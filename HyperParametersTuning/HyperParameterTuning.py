import pandas as pd

import matplotlib.pyplot as plt


from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics  import roc_curve


class HyperParameterTuning :
    def __init__(self,X_train,y_train):
        self.X_train=X_train
        self.y_train=y_train


    """generate a csv file describing different hyper-parameters combination
    and accuracy on each fold for every iteration
    generate also a curve showing the average accuracy on each iteration"""

    def find_best_params(self,params,model,n_iter,algo):
        rsearch = RandomizedSearchCV(estimator=model, param_distributions=params, n_iter=n_iter)
        rsearch.fit(self.X_train, self.y_train)
        cv_results = rsearch.cv_results_
        df = pd.DataFrame.from_dict(cv_results, orient='index')
        df.to_csv('results\/randsearch_'+algo+'.csv')
        plt.plot([i for i in range(n_iter)], cv_results['mean_test_score'])
        plt.xlabel('Number of iterations')
        plt.ylabel('Mean cross-validated accuracy of Random Search')
        plt.savefig('results\/'+algo+'_HyperParameters_Plot')
        plt.clf()
        plt.cla()
        plt.close()
        return rsearch.best_estimator_

    def draw_roc_curve(self,roc_list,X_test,y_test):
        curves=[]
        for i in range(len(roc_list)):
            fpr, tpr, thresholds = roc_curve(y_test, roc_list[i][1].predict_proba(X_test)[:, 1])
            c=[fpr, tpr, thresholds];curves.append(c)
        plt.figure()
        for i in range(len(curves)):
            plt.plot(curves[i][0], curves[i][1], label=roc_list[i][0]+' : (area = %0.2f)' % roc_list[i][2])

        plt.plot([0, 1], [0, 1], label='Base Rate')

        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC Graph')
        plt.legend(loc="lower right")
        plt.savefig('results\Comparison_Plot')







