from sklearn.externals import joblib
import pandas as pd

def predict_manual(ids_list):
    for i in range(len(ids_list)):
        employee_infos = df.loc[df['EmployeeID'] == ids_list[i]]
        employee_infos = employee_infos.drop(['EmployeeID'], axis=1)
        predictions = model_RFC.predict(employee_infos)
        if predictions[0] == 1:
            print('Employee with id= ' + str(ids_list[i]) + ' is NOT EXPECTED TO LEAVE')
        else:
            print('Employee with id= ' + str(ids_list[i]) + ' is EXPECTED TO LEAVE')

def predict_auto(df,ids_list):
    employee_infos = df
    employee_infos = employee_infos.drop(['EmployeeID'], axis=1)
    predictions = model_RFC.predict(employee_infos)
    ids_attr=[]
    for i in range(len(predictions)):
        if predictions[i]==0 : ids_attr.append(ids_list[i])
    print('IDS of employees who are expected to leave : '+str(ids_attr))


#load saved RFC model
model_RFC = joblib.load('..\/ModelDeployment\/results\/model_RFC.joblib')

df  = pd.read_csv("..\/DataPreparation\/results\/out.csv")
df=df.drop_duplicates(subset='EmployeeID',keep="last") #Delete Duplicate(keep last cuz it describes if the employee left or not
df= df.drop(['STATUS'], axis=1)


while True :
    b = input('   Press <a> : to find employees expected to leave\n   Press <m> : to enter IDS manually (ids can be found in database file : database_for_console_test.csv)\n')
    ids_list=[]
    if b=='m':
        s = input("Give an Employee ID Or many IDS (separated by space) :")
        ids_list = list(map(int, s.split()))
        predict_manual(ids_list)
    elif b=='a':
        ids_list = df['EmployeeID'].tolist()
        predict_auto(df, ids_list)




