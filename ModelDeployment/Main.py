import pandas as pd
from ModelDeployment.PredictorModels import  PredictorModels

from sklearn.externals import joblib


df  = pd.read_csv("..\/DataPreparation\/results\/out.csv")
df=df.drop_duplicates(subset='EmployeeID',keep="last") #Delete Duplicate(keep last cuz it describes if the employee left or not

label = df.STATUS
df= df.drop(['STATUS','EmployeeID'], axis=1)
predictor=PredictorModels(df, label)
"""Run the chosen model with the optimal hyper-parameters 
found during the tuning step"""
model_RFC=predictor.RFC(bootstrap= True,criterion='entropy',max_depth=31,
              min_samples_leaf=1,min_samples_split=8,
              n_estimators=25, random_state= 17)


joblib.dump(model_RFC, 'results\/model_RFC.joblib')


predictor.draw_roc_curve()
