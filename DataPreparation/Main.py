import pandas as pd
from DataPreparation.DataFormatter import DataFormatter
import matplotlib.pyplot as plt
import seaborn as sns



df = pd.read_csv("..\/MFG10YearTerminationData.csv")

"""feature engineering """
dataf =DataFormatter()

dataf.jobsToNum(df)

dataf.departmentsToNum(df)

dataf.citiesToNum(df)

dataf.statusToNum(df)


# The gender, business unit and Service_to categories are nominal, so they will
# be exploded instead of being converted to ordinal values
dummy_cols = ['gender_short',  'BUSINESS_UNIT','Service_to']
df = pd.get_dummies(df, columns=dummy_cols)

# Drop record date, birth date,
# termination date, termination reason,termination type(these features are results not predictors)
# gender_full,
# drop job_title (replaced with Hierarchy), department_name (replaced with Service_to),
# and city_name and Pop (replaced with Pop_category)
# DROP age correlated with length _of_service
# DROP BUSINESS_UNIT_HEADOFFICE correlated Service_to_business
# DROP BUSINESS_UNIT_STORES correlated with service_to_customer
# (see initial_heatmap.png)
drop_cols = [ 'recorddate_key', 'birthdate_key', 'orighiredate_key',
             'terminationdate_key', 'gender_full', 'termreason_desc',
             'termtype_desc',   'job_title', 'department_name',
             'city_name', 'Pop','BUSINESS_UNIT_HEADOFFICE','BUSINESS_UNIT_STORES','age']

df = df.drop(drop_cols, axis=1)



#prepared data file
df.to_csv('results\out.csv',index=False)

#check heatmap
labels =  df.columns.tolist()
corr = df.corr()
plt.figure(figsize = (10, 10))
sns.heatmap(corr,
            xticklabels=labels,
            yticklabels=labels,cmap='Blues')
sns.set(font_scale = 3)
plt.savefig('results\plot_final_heatmap')

