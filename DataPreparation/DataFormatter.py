class DataFormatter :

    def jobsToNum(self,df):
        employee = ['Meat Cutter', 'Dairy Person', 'Produce Clerk', 'Baker', 'Cashier',
                    'Shelf Stocker', 'Recruiter', 'HRIS Analyst', 'Accounting Clerk',
                    'Benefits Admin', 'Labor Relations Analyst', 'Accounts Receiveable Clerk',
                    'Accounts Payable Clerk', 'Auditor', 'Compensation Analyst',
                    'Investment Analyst', 'Systems Analyst', 'Corporate Lawyer', 'Legal Counsel']

        manager = ['Customer Service Manager', 'Processed Foods Manager', 'Meats Manager',
                   'Bakery Manager', 'Produce Manager', 'Store Manager', 'Trainer', 'Dairy Manager']

        executive = ['Exec Assistant, Finance', 'Exec Assistant, Legal Counsel',
                     'CHief Information Officer', 'CEO', 'Exec Assistant, Human Resources',
                     'Exec Assistant, VP Stores']

        board = ['VP Stores', 'Director, Recruitment', 'VP Human Resources', 'VP Finance',
                 'Director, Accounts Receivable', 'Director, Accounting',
                 'Director, Employee Records', 'Director, Accounts Payable',
                 'Director, HR Technology', 'Director, Investments',
                 'Director, Labor Relations', 'Director, Audit', 'Director, Training',
                 'Director, Compensation']

        # Make a copy of job titles in a new column
        df['Hierarchy'] = df.job_title

        df.Hierarchy = df.Hierarchy.replace(employee, 0)
        df.Hierarchy = df.Hierarchy.replace(manager, 1)
        df.Hierarchy = df.Hierarchy.replace(executive, 2)
        df.Hierarchy = df.Hierarchy.replace(board, 3)
    def departmentsToNum(self ,df):
        serve_cus = ['Meats', 'Dairy', 'Produce', 'Bakery', 'Customer Service', 'Processed Foods']

        serve_biz = ['Store Management', 'Executive', 'Recruitment', 'HR Technology',
                     'Accounting', 'Employee Records', 'Accounts Receiveable',
                     'Accounts Payable', 'Labor Relations', 'Training', 'Compensation',
                     'Audit', 'Investment', 'Information Technology', 'Legal']
        # Make a copy of department names in a new column
        df['Service_to'] = df.department_name

        # Replace the department names in Service_to
        df.Service_to = df.Service_to.replace(serve_cus, 'Customer')
        df.Service_to = df.Service_to.replace(serve_biz, 'Business')
    def citiesToNum(self,df):
        city_pop_2011 = {'Vancouver': 2313328,
                         'Victoria': 344615,
                         'Nanaimo': 146574,
                         'New Westminster': 65976,
                         'Kelowna': 179839,
                         'Burnaby': 223218,
                         'Kamloops': 85678,
                         'Prince George': 71974,
                         'Cranbrook': 19319,
                         'Surrey': 468251,
                         'Richmond': 190473,
                         'Terrace': 11486,
                         'Chilliwack': 77936,
                         'Trail': 7681,
                         'Langley': 25081,
                         'Vernon': 38180,
                         'Squamish': 17479,
                         'Quesnel': 10007,
                         'Abbotsford': 133497,
                         'North Vancouver': 48196,
                         'Fort St John': 18609,
                         'Williams Lake': 10832,
                         'West Vancouver': 42694,
                         'Port Coquitlam': 55985,
                         'Aldergrove': 12083,
                         'Fort Nelson': 3561,
                         'Nelson': 10230,
                         'New Westminister': 65976,
                         'Grand Forks': 3985,
                         'White Rock': 19339,
                         'Haney': 76052,
                         'Princeton': 2724,
                         'Dawson Creek': 11583,
                         'Bella Bella': 1095,
                         'Ocean Falls': 129,
                         'Pitt Meadows': 17736,
                         'Cortes Island': 1007,
                         'Valemount': 1020,
                         'Dease Lake': 58,
                         'Blue River': 215}
        # Make a copy of city names
        df['Pop'] = df.city_name

        # Map from city name to population
        df.Pop = df.Pop.map(city_pop_2011)

        # Make a new column for population category
        df['Pop_category'] = df.Pop

        # Categorise according to population size
        # >= 100,000 is City
        # 10,000 to 99,999 is Rural
        # < 10,000 is Remote
        # Guidance from Australian Institute of Health and Welfare
        # http://www.aihw.gov.au/rural-health-rrma-classification/
        city_ix = (df['Pop'] >= 100000)
        rural_ix = ((df['Pop'] < 100000) & (df['Pop'] >= 10000))
        remote_ix = (df['Pop'] < 10000)
        df.loc[city_ix, 'Pop_category'] = 'City'
        df.loc[rural_ix, 'Pop_category'] = 'Rural'
        df.loc[remote_ix, 'Pop_category'] = 'Remote'

        df.Pop_category = df.Pop_category.replace('Remote', 0)
        df.Pop_category = df.Pop_category.replace('Rural', 1)
        df.Pop_category = df.Pop_category.replace('City', 2)

    def statusToNum(self,df):
        df.STATUS = df.STATUS.map({'ACTIVE': 1, 'TERMINATED': 0})