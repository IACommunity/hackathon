
import pandas as pd
from HyperParametersTuning.HyperParameterTuning import HyperParameterTuning
from sklearn.metrics  import classification_report,confusion_matrix,roc_auc_score
from scipy.stats import randint as sp_randint
from scipy import stats
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC

df1 = pd.read_csv("..\/DataPreparation\/results\/out.csv")
df1=df1.drop_duplicates(subset='EmployeeID',keep="last") #Delete Duplicate(keep last cuz it describes if the employee left or not
label = df1.STATUS
df1 = df1.drop(['STATUS','EmployeeID'], axis=1)


"""
keep 30% of dataset as test data to check the performance of the models
the rest of data is given to HyperParameterTuner where RandomizedSearchCV uses cross validation 
to find the best hyperparameters
"""
X_train, X_test, y_train, y_test = train_test_split(df1, label, test_size=0.3,random_state=10)
hpt = HyperParameterTuning(X_train,y_train)

n_iter=100#nbr of combinations of hyperparameters

models=[]
"""finding hyperparameters for KNN"""
model_KNN = KNeighborsClassifier()
params = {    "weights":['uniform','distance'],
              "n_neighbors":sp_randint(1, 15),
              "algorithm": ['auto', 'ball_tree', 'kd_tree', 'brute'],
              "p": sp_randint(1, 20)
             }
print("Searching KNN hyper-parameters....")
best_model_KNN = hpt.find_best_params(params,model_KNN,n_iter,'KNN')
models.append(['KNN',best_model_KNN])
print("Optimal hyper-parameters found for KNN :")
print(best_model_KNN.get_params())

"""finding hyperparameters for RFC"""
model_RFC = RandomForestClassifier()
params = {    "n_estimators":sp_randint(11, 99),
              "max_depth": sp_randint(1, 40),
              "min_samples_split": sp_randint(2, 11),
              "min_samples_leaf": sp_randint(1, 11),
              "bootstrap": [True, False],
              "criterion": ["gini", "entropy"],
              "random_state" : sp_randint(5, 20),
             }
print("Searching RFC hyper-parameters....")
best_model_RFC = hpt.find_best_params(params,model_RFC,n_iter,'RFC')
models.append(['RFC',best_model_RFC])
print("Optimal hyper-parameters found for RFC :")
print(best_model_RFC.get_params())

"""Finding hyper-parameters for svc"""
model_SVC= SVC(probability=True)
params = {
          "C": stats.uniform(2, 10),
          "gamma": stats.uniform(0.1, 1)}
print("Searching SVC hyper-parameters....")
best_model_SVC = hpt.find_best_params(params,model_SVC,n_iter,'SVC')
models.append(['SVC',best_model_SVC])
print("Optimal hyper-parameters found for SVC :")
print(best_model_SVC.get_params())

"""Testing best estimators with the testint dataset"""
for i in range(len(models)):
    print("Predictions of ",models[i][0])
    predictions = models[i][1].predict(X_test)
    print("Confusion Matrix: ")
    print(confusion_matrix(y_test, predictions))
    print(classification_report(y_test, predictions))
    models[i].append(roc_auc_score(y_test, predictions))
"""Comparing models"""
hpt.draw_roc_curve(models,X_test,y_test)
