
The project is composed of three modules :
	The project is composed of four modules (THE MODULES CAN BE EXECUTED INDEPENDENTLY):
	1) DataPreparation : Where the data is loaded from the database (MFG10YearTerminationData.csv),
	prepared and stored in (results\out.csv) with the corresponding heatmap.
	
	2) HyperParametersTuning : Where a research for hyper-parameters for the models is performed.
	The comparison of the accuracy of the models is performed at the end. 
	The results of this research and comparison are stored in (results) directory.
	Obviously, this module will take time to find hyper-parameters.
	
	3) ModelDeployment : In this module the RFC model is trained with the optimal hyper-parameters found in the
	previous module.
	The model is then stored as a joblib in (results\model_RFC.joblib).
	
	4) ModelExecution : Here where our project is executed. After lunching Main.py of this module, the (results\model_RFC.joblib)
	is loaded, and then the program will ask you to choose between two options :
		-Option 1 : Finding in database employees who will leave. For this option press the keyboard button "a". 
		-Option 2 : Giving Ids of employees to the program so it can check for you if these 
		employees will leave or not. For this option you press the keyboard button "m".
	note : To test Option 2 ,you can find ids of employees in the file (database_for_console_test.csv).