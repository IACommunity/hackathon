from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics  import classification_report,confusion_matrix,roc_auc_score,roc_curve
import matplotlib.pyplot as plt

class PredictorModels:

    def __init__(self,data,label,_size=0.3,_rand_state=10):
        self.roc_list=[]
        self.models=[]
        self.data=data
        self.X_train, self.X_test, self.y_train, self.y_test = \
            train_test_split(data, label, test_size=_size,random_state=_rand_state)



    def RFC(self,n_estimators, criterion, min_samples_split, random_state,max_depth,min_samples_leaf,bootstrap):
        model = RandomForestClassifier(n_estimators=n_estimators, criterion=criterion,
                                       min_samples_split=min_samples_split,
                                       random_state=random_state,max_depth=max_depth,
                                       min_samples_leaf=min_samples_leaf,
                                       bootstrap=bootstrap)
        model.fit(self.X_train, self.y_train)
        score = model.score(self.X_test, self.y_test)
        self.models.append(('RFC',model))
        print('Random Forest model score is %0.4f' % score)
        predictions = model.predict(self.X_test)
        print("Confusion Matrix: ")
        print(confusion_matrix(self.y_test, predictions))
        print(classification_report(self.y_test, predictions))
        self.roc_list.append(['RFC',model, roc_auc_score(self.y_test, predictions)])
        return model





    def draw_roc_curve(self):
        curves=[]
        for i in range(len(self.roc_list)):
            print('iteration ',i)
            fpr, tpr, thresholds = roc_curve(self.y_test, self.roc_list[i][1].predict_proba(self.X_test)[:, 1])
            c=[fpr, tpr, thresholds];curves.append(c)
        plt.figure()
        for i in range(len(curves)):
            plt.plot(curves[i][0], curves[i][1], label=self.roc_list[i][0]+' : (area = %0.2f)' % self.roc_list[i][2])

        plt.plot([0, 1], [0, 1], label='Base Rate')

        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC Graph')
        plt.legend(loc="lower right")
        plt.show()

